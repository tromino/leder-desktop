namespace Leder {
	public class State: Object {
		public enum ModalType {
			NONE,
			TEST_DIALOG
		}
		
		private Storage.Database database;
		private Storage.MediaStore media;
		public ColorProvider color_provider { get; set; }
		public ModalType open_modal { get; set; default = NONE; }
		public Gtk.ListStore communities { get; private set; }
		
		construct {
			this.database = new Storage.Database();
			this.media = new Storage.MediaStore();
			this.color_provider = new ColorProvider();
			this.color_provider.color_theme = ColorProvider.dark_theme;
		}
		
		public bool init() {
			return this.database.init() && this.media.init();
		}
	}
}
