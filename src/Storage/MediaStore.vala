namespace Leder.Storage {
	public class MediaStore: Object {
		private string cache_dir;
		
		public bool init() {
			this.cache_dir = Environment.get_user_cache_dir()
				+ "/leder";
			
			var cache_dir_res = DirUtils.create_with_parents(cache_dir, 0700);
			
			if(cache_dir_res != 0) {
				printerr("Failed to create cache folder\n");
				
				return false;
			}
			
			return true;
		}
	}
}
