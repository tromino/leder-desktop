namespace Leder.Storage {
	public class Database: Object {
		private Sqlite.Database database;
		private string data_dir;
		
		public bool init() {
			this.data_dir = Environment.get_user_data_dir()
				+ "/leder";
			
			var data_dir_res = DirUtils.create_with_parents(data_dir, 0700);
			
			if(data_dir_res != 0) {
				printerr("Failed to create data folder\n");
				
				return false;
			}
			
			// Set up database
			var db_exists = GLib.FileUtils.test(
				data_dir + "/session.db",
				FileTest.EXISTS
			);
			
			var db_res = Sqlite.Database.open(
				data_dir + "/session.db",
				out this.database
			);
			
			if(db_res != Sqlite.OK) {
				printerr("Failed to open database\n");
				
				return false;
			}
			
			// Initialize database if just created
			if(!db_exists) {
				var query = """
					CREATE TABLE file (
						id BYTEA NOT NULL PRIMARY KEY,
						extension VARCHAR(100),
						filename VARCHAR(100),
						last_accessed timestamp(0) NOT NULL
					);
					
					CREATE TABLE user (
						id BYTEA NOT NULL PRIMARY KEY,
						display_name VARCHAR(50) NOT NULL,
						avatar BYTEA,
						FOREIGN KEY (avatar) REFERENCES file(id)
					);
					
					CREATE TABLE message (
						id BYTEA NOT NULL PRIMARY KEY,
						author BYTEA NOT NULL,
						content VARCHAR(5000),
						attachment INT,
						FOREIGN KEY (author) REFERENCES user(id),
						FOREIGN KEY (attachment) REFERENCES file(id)
					);
					
					CREATE TABLE dm (
						id BYTEA NOT NULL PRIMARY KEY,
						with_user BYTEA NOT NULL,
						FOREIGN KEY (with_user) REFERENCES user(id)
					);
					
					CREATE TABLE dm_message (
						dm BYTEA NOT NULL,
						message BYTEA NOT NULL PRIMARY KEY,
						FOREIGN KEY (dm) REFERENCES dm(id),
						FOREIGN KEY (message) REFERENCES message(id)
					);
					
					CREATE TABLE lounge (
						id BYTEA NOT NULL PRIMARY KEY,
						display_name VARCHAR(50) NOT NULL,
						description VARCHAR(250),
						avatar BYTEA,
						FOREIGN KEY (avatar) REFERENCES file(id)
					);
					
					CREATE TABLE lounge_message (
						lounge BYTEA NOT NULL,
						message BYTEA NOT NULL PRIMARY KEY,
						FOREIGN KEY (lounge) REFERENCES lounge(id),
						FOREIGN KEY (message) REFERENCES message(id)
					);
					
					CREATE TABLE lounge_user (
						lounge BYTEA NOT NULL,
						user BYTEA NOT NULL,
						PRIMARY KEY (lounge, user),
						FOREIGN KEY (lounge) REFERENCES lounge(id),
						FOREIGN KEY (user) REFERENCES user(id)
					);
					
					CREATE TABLE channel (
						id BYTEA NOT NULL PRIMARY KEY,
						display_name VARCHAR(50) NOT NULL,
						description VARCHAR(250)
					);
					
					CREATE TABLE channel_message (
						channel BYTEA NOT NULL,
						message BYTEA NOT NULL PRIMARY KEY,
						FOREIGN KEY (channel) REFERENCES channel(id),
						FOREIGN KEY (message) REFERENCES message(id)
					);
					
					CREATE TABLE community (
						id BYTEA NOT NULL PRIMARY KEY,
						display_name VARCHAR(50) NOT NULL,
						avatar BYTEA,
						FOREIGN KEY (avatar) REFERENCES file(id)
					);
					
					CREATE TABLE community_user (
						community BYTEA NOT NULL,
						user BYTEA NOT NULL,
						PRIMARY KEY (community, user),
						FOREIGN KEY (community) REFERENCES community(id),
						FOREIGN KEY (user) REFERENCES user(id)
					);
					
					CREATE TABLE community_channel (
						community BYTEA NOT NULL,
						channel BYTEA NOT NULL PRIMARY KEY,
						FOREIGN KEY (community) REFERENCES community(id),
						FOREIGN KEY (channel) REFERENCES channel(id)
					);
				""";
				
				var query_res = this.database.exec(query, null, null);
				
				if(query_res != Sqlite.OK) {
					printerr("Failed to populate database\n");
					
					return false;
				}
			}
			
			return true;
		}
	}
}
