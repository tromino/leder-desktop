namespace Leder {
	public struct ColorTheme {
		string accent_bg;
		string accent_fg;
		
		string destruct_bg;
		string destruct_fg;
		
		string window_bg;
		string window_fg;
		string sidebar_bg;
		string sidebar_fg;
		string content_bg;
		string content_fg;
		string subcontent_bg;
		string subcontent_fg;
		string supercontent_bg;
		string supercontent_fg;
		string textarea_bg;
		string textarea_fg;
		string menu_bg;
		string menu_fg;
		string tooltip_bg;
		string tooltip_fg;
	}
}
