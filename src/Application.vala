namespace Leder {
	public class Application: Gtk.Application {
		private Widgets.MainWindow window;
		public State state { get; set; }
		
		construct {
			this.application_id = "org.neocities.tromino.Leder";
			this.state = new State();
			
			this.activate.connect(() => {
				var state_res = state.init();
				
				// Will be false if there were errors
				if(state_res) {
					this.window = new Widgets.MainWindow(this);
					
					// Add generated CSS colors
					Gtk.StyleContext.add_provider_for_display(
						this.window.get_display(),
						this.state.color_provider.css_provider,
						Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
					);
					
					// Add the app stylesheet
					var css_provider = new Gtk.CssProvider();
					css_provider.load_from_resource("/org/neocities/tromino/Leder/stylesheet.css");
					
					Gtk.StyleContext.add_provider_for_display(
						this.window.get_display(),
						css_provider,
						Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
					);
					
					// Set up actions
					var quit_action = new SimpleAction("quit", null);
					var open_test_dialog_modal_action = new SimpleAction("open-test-dialog-modal", null);
					var close_modal_action = new SimpleAction("close-modal", null);
					var set_light_theme_action = new SimpleAction("set-light-theme", null);
					var set_dark_theme_action = new SimpleAction("set-dark-theme", null);
					var set_sparkle_theme_action = new SimpleAction("set-sparkle-theme", null);
					
					quit_action.activate.connect(() => {
						this.quit();
					});
					
					open_test_dialog_modal_action.activate.connect(() => {
						this.state.open_modal = TEST_DIALOG;
					});
					
					close_modal_action.activate.connect(() => {
						this.state.open_modal = NONE;
					});
					
					set_light_theme_action.activate.connect(() => {
						this.state.color_provider.color_theme = ColorProvider.light_theme;
					});
					
					set_dark_theme_action.activate.connect(() => {
						this.state.color_provider.color_theme = ColorProvider.dark_theme;
					});
					
					set_sparkle_theme_action.activate.connect(() => {
						this.state.color_provider.color_theme = ColorProvider.sparkle_theme;
					});
					
					this.add_action(quit_action);
					this.add_action(open_test_dialog_modal_action);
					this.add_action(close_modal_action);
					this.add_action(set_light_theme_action);
					this.add_action(set_dark_theme_action);
					this.add_action(set_sparkle_theme_action);
					
					this.set_accels_for_action("app.quit", { "<Primary>Q" });
					this.set_accels_for_action("app.set-light-theme", { "<Primary>E" });
					this.set_accels_for_action("app.set-dark-theme", { "<Primary>N" });
					this.set_accels_for_action("app.set-sparkle-theme", { "<Primary>I" });
					this.set_accels_for_action("app.close-modal", { "Escape" });
					
					this.window.present();
				} else {
					printerr("Can't initialize local storage, exiting...\n");
				}
			});
		}
		
		public static int main(string[] args) {
			var app = new Application();
			
			return app.run(args);
		}
	}
}
