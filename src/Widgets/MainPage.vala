namespace Leder.Widgets {
	[GtkTemplate(ui = "/org/neocities/tromino/Leder/templates/MainPage.ui")]
	public class MainPage: Gtk.Box {
		public State state { get; set; }
		
		static construct {
			typeof(Constrainer).ensure();
			typeof(CommunityPanel).ensure();
			typeof(MainSidebar).ensure();
			typeof(MainContent).ensure();
		}
	}
}
