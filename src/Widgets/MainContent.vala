namespace Leder.Widgets {
	[GtkTemplate(ui = "/org/neocities/tromino/Leder/templates/MainContent.ui")]
	public class MainContent: Gtk.Box {
		[GtkChild]
		private unowned Gtk.TextView message_textview;
		public State state { get; set; }
		
		static construct {
			typeof(Constrainer).ensure();
		}
		
		class construct {
			set_css_name("maincontent");
		}
		
		construct {
			this.message_textview.realize.connect(() => {
				this.message_textview.grab_focus();
			});
		}
	}
}
