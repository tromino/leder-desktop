namespace Leder.Widgets {
	[GtkTemplate(ui = "/org/neocities/tromino/Leder/templates/CommunityPanel.ui")]
	public class CommunityPanel: Gtk.Box {
		public State state { get; set; }
		
		class construct {
			set_css_name("communitypanel");
		}
	}
}
