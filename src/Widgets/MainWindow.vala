namespace Leder.Widgets {
	[GtkTemplate(ui = "/org/neocities/tromino/Leder/templates/MainWindow.ui")]
	public class MainWindow: Gtk.ApplicationWindow {
		[GtkChild]
		private unowned Gtk.Stack main_stack;
		[GtkChild]
		private unowned MainPage main_page;
		public State state { get; set; }
		
		static construct {
			typeof(BindSpacer).ensure();
			typeof(ModalWindow).ensure();
			typeof(LoginPage).ensure();
			typeof(MainPage).ensure();
		}
		
		public MainWindow(Application application) {
			Object(
				application: application,
				state: application.state
			);
		}
		
		construct {
			this.add_direction_class(this.get_direction());
			
			this.direction_changed.connect((previous_direction) => {
				this.remove_direction_class(previous_direction);
				this.add_direction_class(this.get_direction());
			});
			
			Timeout.add_seconds(1, () => {
				this.main_stack.visible_child = this.main_page;
				
				return Source.REMOVE;
			});
		}
		
		private void add_direction_class(
			Gtk.TextDirection direction
		) {
			switch(direction) {
				case LTR:
					this.add_css_class("ltr");
					
					break;
				
				case RTL:
					this.add_css_class("rtl");
					
					break;
				
				default:
					break;
			}
		}
		
		private void remove_direction_class(
			Gtk.TextDirection direction
		) {
			switch(direction) {
				case LTR:
					this.remove_css_class("ltr");
					
					break;
				
				case RTL:
					this.remove_css_class("rtl");
					
					break;
				
				default:
					break;
			}
		}
	}
}
