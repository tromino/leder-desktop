namespace Leder.Widgets {
	public class BindSpacer: Gtk.Widget {
		public Gtk.Widget reference { get; construct; }
		
		class construct {
			set_css_name("bindspacer");
		}
		
		// TODO: Watch for sizing changes somehow?
		// construct {
		// 	this.reference.meow_meow_meow.connect(() => {
		// 		this.queue_resize();
		// 	});
		// }
		
		public override void measure(
			Gtk.Orientation orientation,
			int for_size,
			out int minimum,
			out int natural,
			out int minimum_baseline,
			out int natural_baseline
		) {
			this.reference.measure(
				orientation,
				for_size,
				out minimum,
				out natural,
				out minimum_baseline,
				out natural_baseline
			);
		}
	}
}
