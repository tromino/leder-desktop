namespace Leder.Widgets {
	public class Constrainer: Gtk.Widget {
		public enum SizingMode {
			FORCE,
			STRETCH,
			CONSTRAIN
		}
		
		public int width { get; set; default = -1; }
		public int height { get; set; default = -1; }
		
		public int min_width { get; set; default = -1; }
		public int min_height { get; set; default = -1; }
		
		public int max_width { get; set; default = -1; }
		public int max_height { get; set; default = -1; }
		
		public SizingMode sizing_mode { get; set; default = CONSTRAIN; }
		
		class construct {
			set_css_name("constrainer");
		}
		
		construct {
			this.notify["width"].connect(() => {
				this.queue_resize();
			});
			
			this.notify["height"].connect(() => {
				this.queue_resize();
			});
			
			this.notify["min-width"].connect(() => {
				this.queue_resize();
			});
			
			this.notify["min-height"].connect(() => {
				this.queue_resize();
			});
			
			this.notify["max-width"].connect(() => {
				this.queue_resize();
			});
			
			this.notify["max-height"].connect(() => {
				this.queue_resize();
			});
		}
		
		public override void dispose() {
			var children = new Gtk.Widget[0];
			
			for(
				var child = this.get_first_child();
				child != null;
				child = ((!) child).get_next_sibling()
			) {
				children += (!) child;
			}
			
			foreach(var child in children) {
				child.unparent();
			}
		}
		
		public override void measure(
			Gtk.Orientation orientation,
			int for_size,
			out int minimum,
			out int natural,
			out int minimum_baseline,
			out int natural_baseline
		) {
			var ret_minimum = 0;
			var ret_natural = 0;
			var ret_minimum_baseline = -1;
			var ret_natural_baseline = -1;
			
			var first_child = this.get_first_child();
			
			if(first_child != null) {
				((!) first_child).measure(
					orientation,
					for_size,
					out ret_minimum,
					out ret_natural,
					out ret_minimum_baseline,
					out ret_natural_baseline
				);
			}
			
			switch(orientation) {
				case HORIZONTAL:
					if(this.width > -1) {
						ret_minimum = this.width;
						ret_natural = this.width;
					} else if(
						(this.min_width < 0 || this.max_width < 0)
							|| this.min_width <= this.max_width
					) {
						switch(this.sizing_mode) {
							case FORCE:
								if(this.min_width > -1) {
									ret_minimum = this.min_width;
								}
								
								if(this.max_width > -1) {
									ret_natural = this.max_width;
								}
								
								break;
							
							case STRETCH:
								if(this.min_width > -1) {
									ret_minimum = int.min(
										ret_minimum,
										this.min_width
									);
								}
								
								if(this.max_width > -1) {
									ret_natural = int.max(
										ret_natural,
										this.max_width
									);
								}
								
								break;
							
							default:
							case CONSTRAIN:
								if(this.min_width > -1) {
									ret_minimum = int.max(
										ret_minimum,
										this.min_width
									);
									
									ret_natural = int.max(
										ret_natural,
										this.min_width
									);
								}
								
								if(this.max_width > -1) {
									ret_minimum = int.min(
										ret_minimum,
										this.max_width
									);
									
									ret_natural = int.min(
										ret_natural,
										this.max_width
									);
								}
								
								break;
						}
					} else {
						warning(
							"Minimum width %i is higher than maximum width %i",
							this.min_width,
							this.max_width
						);
					}
					
					break;
				
				default:
				case VERTICAL:
					if(this.height > -1) {
						ret_minimum = this.height;
						ret_natural = this.height;
					} else if(
						(this.min_height < 0 || this.max_height < 0)
							|| this.min_height <= this.max_height
					) {
						switch(this.sizing_mode) {
							case FORCE:
								if(this.min_height > -1) {
									ret_minimum = this.min_height;
								}
								
								if(this.max_height > -1) {
									ret_natural = this.max_height;
								}
								
								break;
							
							case STRETCH:
								if(this.min_height > -1) {
									ret_minimum = int.min(
										ret_minimum,
										this.min_height
									);
								}
								
								if(this.max_height > -1) {
									ret_natural = int.max(
										ret_natural,
										this.max_height
									);
								}
								
								break;
							
							default:
							case CONSTRAIN:
								if(this.min_height > -1) {
									ret_minimum = int.max(
										ret_minimum,
										this.min_height
									);
									
									ret_natural = int.max(
										ret_natural,
										this.min_height
									);
								}
								
								if(this.max_height > -1) {
									ret_minimum = int.min(
										ret_minimum,
										this.max_height
									);
									
									ret_natural = int.min(
										ret_natural,
										this.max_height
									);
								}
								
								break;
						}
					} else {
						warning(
							"Minimum height %i is higher than maximum height %i",
							this.min_height,
							this.max_height
						);
					}
					
					break;
			}
			
			minimum = ret_minimum;
			natural = ret_natural;
			minimum_baseline = ret_minimum_baseline;
			natural_baseline = ret_natural_baseline;
		}
		
		public override void size_allocate(
			int width,
			int height,
			int baseline
		) {
			var children = new Gtk.Widget[0];
			
			for(
				var child = this.get_first_child();
				child != null;
				child = ((!) child).get_next_sibling()
			) {
				children += (!) child;
			}
			
			foreach(var child in children) {
				child.allocate(
					width,
					height,
					baseline,
					new Gsk.Transform().translate((!) Graphene.Point.zero())
				);
			}
		}
	}
}
