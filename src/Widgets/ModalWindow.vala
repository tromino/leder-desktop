namespace Leder.Widgets {
	[GtkTemplate(ui = "/org/neocities/tromino/Leder/templates/ModalWindow.ui")]
	public class ModalWindow: Gtk.Box, Gtk.Buildable {
		// [GtkChild]
		// private unowned Gtk.Box modal_content;
		private bool open = false;
		public State state { get; set; }
		
		class construct {
			set_css_name("modalwindow");
		}
		
		construct {
			// TODO: Listen to a more appropriate signal?
			this.realize.connect(() => {
				this.state.notify["open-modal"].connect(() => {
					switch(this.state.open_modal) {
						case NONE:
							if(this.open) {
								this.open = false;
								this.can_target = false;
								this.remove_css_class("open");
							}
							
							break;
						
						default:
						case TEST_DIALOG:
							if(!this.open) {
								this.open = true;
								this.can_target = true;
								this.add_css_class("open");
							}
							
							break;
					}
				});
			});
		}
		
		public void add_child(
			Gtk.Builder builder,
			Object child,
			string? type
		) {
			if(type != null && ((!) type) == "spacer") {
				this.prepend((Gtk.Widget) child);
			} else {
				this.append((Gtk.Widget) child);
			}
		}
	}
}
