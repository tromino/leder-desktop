namespace Leder.Widgets {
	[GtkTemplate(ui = "/org/neocities/tromino/Leder/templates/MainSidebar.ui")]
	public class MainSidebar: Gtk.Box {
		public State state { get; set; }
		
		class construct {
			set_css_name("mainsidebar");
		}
	}
}
