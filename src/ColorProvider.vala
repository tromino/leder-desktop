namespace Leder {
	public class ColorProvider: Object {
		public static ColorTheme dark_theme { get; private set; }
		public static ColorTheme light_theme { get; private set; }
		public static ColorTheme sparkle_theme { get; private set; }
		
		public Gtk.CssProvider css_provider { get; private set; }
		public ColorTheme color_theme { get; set; }
		
		static construct {
			dark_theme = ColorTheme() {
				accent_bg = "#6da965",
				accent_fg = "#ffffff",
				destruct_bg = "#ee545b",
				destruct_fg = "#ffffff",
				window_bg = "#181818",
				window_fg = "#ffffff",
				sidebar_bg = "#1e1e1e",
				sidebar_fg = "#ffffff",
				content_bg = "#262626",
				content_fg = "#ffffff",
				subcontent_bg = "#1e1e1e",
				subcontent_fg = "#ffffff",
				supercontent_bg = "#2f2f2f",
				supercontent_fg = "#ffffff",
				textarea_bg = "#1e1e1e",
				textarea_fg = "#ffffff",
				menu_bg = "#181818",
				menu_fg = "#ffffff",
				tooltip_bg = "#000000",
				tooltip_fg = "#ffffff"
			};
			
			light_theme = ColorTheme() {
				accent_bg = "#6da965",
				accent_fg = "#ffffff",
				destruct_bg = "#ee545b",
				destruct_fg = "#ffffff",
				window_bg = "#181818",
				window_fg = "#ffffff",
				sidebar_bg = "#1e1e1e",
				sidebar_fg = "#ffffff",
				content_bg = "#f6f6f6",
				content_fg = "#000000",
				subcontent_bg = "#e8e8e8",
				subcontent_fg = "#000000",
				supercontent_bg = "#ffffff",
				supercontent_fg = "#000000",
				textarea_bg = "#e8e8e8",
				textarea_fg = "#000000",
				menu_bg = "#ffffff",
				menu_fg = "#000000",
				tooltip_bg = "#000000",
				tooltip_fg = "#ffffff"
			};
			
			sparkle_theme = ColorTheme() {
				accent_bg = "#fefb21",
				accent_fg = "#0419f2",
				destruct_bg = "#74a1fa",
				destruct_fg = "#432c1a",
				window_bg = "#541e59",
				window_fg = "#fffe9a",
				sidebar_bg = "#8c2293",
				sidebar_fg = "#f9dbd9",
				content_bg = "#d4fafc",
				content_fg = "#234a29",
				subcontent_bg = "#ead4e6",
				subcontent_fg = "#42090a",
				supercontent_bg = "#fafbfd",
				supercontent_fg = "#222d57",
				textarea_bg = "#94e697",
				textarea_fg = "#320439",
				menu_bg = "#3529db",
				menu_fg = "#f1eaef",
				tooltip_bg = "#402a4a",
				tooltip_fg = "#ebaea8"
			};
		}
		
		construct {
			this.css_provider = new Gtk.CssProvider();
			
			this.notify["color-theme"].connect(() => {
				var css_string = """
					@define-color accent-bg %s;
					@define-color accent-fg %s;
					
					@define-color destruct-bg %s;
					@define-color destruct-fg %s;
					
					@define-color window-bg %s;
					@define-color window-fg %s;
					@define-color sidebar-bg %s;
					@define-color sidebar-fg %s;
					@define-color content-bg %s;
					@define-color content-fg %s;
					@define-color subcontent-bg %s;
					@define-color subcontent-fg %s;
					@define-color supercontent-bg %s;
					@define-color supercontent-fg %s;
					@define-color textarea-bg %s;
					@define-color textarea-fg %s;
					@define-color menu-bg %s;
					@define-color menu-fg %s;
					@define-color tooltip-bg %s;
					@define-color tooltip-fg %s;
				""".printf(
					this.color_theme.accent_bg,
					this.color_theme.accent_fg,
					this.color_theme.destruct_bg,
					this.color_theme.destruct_fg,
					this.color_theme.window_bg,
					this.color_theme.window_fg,
					this.color_theme.sidebar_bg,
					this.color_theme.sidebar_fg,
					this.color_theme.content_bg,
					this.color_theme.content_fg,
					this.color_theme.subcontent_bg,
					this.color_theme.subcontent_fg,
					this.color_theme.supercontent_bg,
					this.color_theme.supercontent_fg,
					this.color_theme.textarea_bg,
					this.color_theme.textarea_fg,
					this.color_theme.menu_bg,
					this.color_theme.menu_fg,
					this.color_theme.tooltip_bg,
					this.color_theme.tooltip_fg
				);
				
				this.css_provider.load_from_data(css_string.data);
			});
		}
	}
}
